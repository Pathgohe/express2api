const Product = require('../../models/v2/Product')
const auth = require('../../middlewares/auth')

const index = (req, res) => {
  Product.find((err, product) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo el producto'
      })
    }
    return res.json(product)
  })
}

const create = (req, res) => {
  const product = new Product(req.body)
  product.save((err, product) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar el producto',
        error: err
      })
    }
    return res.status(201).json(product)
  })
}
const { ObjectId } = require('mongodb')
const show = (req, res) => {
  const id = req.params.id
  Product.findOne({ _id: id }, (err, product) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).json({ mensaje: 'id no valido' })
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al obtener el producto'
      })
    }
    if (!product) {
      return res.status(404).json({
        message: 'No tenemos este producto'
      })
    }
    return res.json(product)
  })
}

const update = (req, res) => {
  const id = req.params.id
  Product.findOne({ _id: id }, (err, product) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al guardar la cerveza',
        error: err
      })
    }

    Object.assign(product, req.body)

    product.save((err, product) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al guardar la product'
        })
      }
      if (!product) {
        return res.status(404).json({
          message: 'No hemos encontrado la product'
        })
      }
      return res.json(product)
    })
  })
}
const destroy = (req, res) => {
  const id = req.params.id

  Product.findOneAndDelete({ _id: id }, (err, product) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.json(500, {
        message: 'No hemos encontrado la product'
      })
    }
    if (!product) {
      return res.status(404).json({
        message: 'No hemos encontrado la product'
      })
    }

    return res.json(product)
  })
}
module.exports = {
  index,
  create,
  show,
  update,
  destroy
}
