const Bebida = require('../../models/v2/Bebida')

const index = (req, res) => {
    Bebida.find((err, bebida) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo la cerveza'
      })
    }
    return res.json(bebida)
  })
}

const { ObjectId } = require('mongodb')
const show = (req, res) => {
  const id = req.params.id
  Bebida.findOne({ _id: id }, (err, bebida) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).json({ mensaje: 'id no valido' })
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al obtener la bebida'
      })
    }
    if (!bebida) {
      return res.status(404).json({
        message: 'No tenemos esta bebida'
      })
    }
    return res.json(bebida)
  })
}
const create = (req, res) => {
  const bebida = new Bebida(req.body)
  bebida.save((err, bebida) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar la bebida',
        error: err
      })
    }
    return res.status(201).json(bebida)
  })
}

const update = (req, res) => {
  const id = req.params.id
  Bebida.findOne({ _id: id }, (err, bebida) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al guardar la cerveza',
        error: err
      })
    }

    Object.assign(bebida, req.body)

    bebida.save((err, bebida) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al guardar la cerveza'
        })
      }
      if (!bebida) {
        return res.status(404).json({
          message: 'No hemos encontrado la cerveza'
        })
      }
      return res.json(bebida)
    })
  })
}
const remove = (req, res) => {
  const id = req.params.id

  Bebida.findOneAndDelete({ _id: id }, (err, bebida) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.json(500, {
        message: 'No hemos encontrado la cerveza'
      })
    }
    if (!bebida) {
      return res.status(404).json({
        message: 'No hemos encontrado la cerveza'
      })
    }
    return res.json(bebida)
  })
}
module.exports = {
  index,
  show,
  create,
  update,
  remove
}
