const Cathegory = require('../../models/v2/Cathegory')

const index = (req, res) => {
  Cathegory.find((err, cathegory) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo la cerveza'
      })
    }
    return res.json(cathegory)
  })
}

const { ObjectId } = require('mongodb')
const show = (req, res) => {
  const id = req.params.id
  Cathegory.findOne({ _id: id }, (err, cathegory) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).json({ mensaje: 'id no valido' })
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al obtener la cerveza'
      })
    }
    if (!cathegory) {
      return res.status(404).json({
        message: 'No tenemos esta cerveza'
      })
    }
    return res.json(cerveza)
  })
}
const create = (req, res) => {
  const cathegory = new Cathegory(req.body)
  cathegory.save((err, cathegory) => {
    if (err) {
      return res.status(400).json({
        message: 'Error al guardar la cerveza',
        error: err
      })
    }
    return res.status(201).json(cathegory)
  })
}

const update = (req, res) => {
  const id = req.params.id
  Cathegory.findOne({ _id: id }, (err, cathegory) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.status(500).json({
        message: 'Se ha producido un error al guardar la cerveza',
        error: err
      })
    }

    Object.assign(cathegory, req.body)

    cathegory.save((err, cathegory) => {
      if (err) {
        return res.status(500).json({
          message: 'Error al guardar la cerveza'
        })
      }
      if (!cathegory) {
        return res.status(404).json({
          message: 'No hemos encontrado la cerveza'
        })
      }
      return res.json(cathegory)
    })
  })
}
const remove = (req, res) => {
  const id = req.params.id

  Cathegory.findOneAndDelete({ _id: id }, (err, cathegory) => {
    if (!ObjectId.isValid(id)) {
      return res.status(404).send()
    }
    if (err) {
      return res.json(500, {
        message: 'No hemos encontrado la cerveza'
      })
    }
    if (!cathegory) {
      return res.status(404).json({
        message: 'No hemos encontrado la cerveza'
      })
    }
    return res.json(cathegory)
  })
}
module.exports = {
  index,
  show,
  create,
  update,
  remove
}
