// const mongoose = require('mongoose')
const User = require('../../models/v2/User')
const servicejwt = require('../../services/servicejwt')

const index = (req, res) => {
  User.find((err, users) => {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo el usuario'
      })
    }
    return res.json(users)
  })
}

const register = (req, res) => {
  // return res.json({ message: 'alta de usuario' })
  const user = new User({
    email: req.body.email,
    name: req.body.name,
    password: req.body.password
  })

  user.save(err => {
    if (err) res.status(500).send({ message: `Error al crear usuario: ${err}` })
    // servicejwt nos va a crear un token
    return res.status(200).send({ token: servicejwt.createToken(user) })
  })
}
const login = (req, res) => {
  User.findOne({ email: req.body.email.toLowerCase() }, function (err, user) {
    if (err) {
      return res.status(500).json({
        message: 'Error obteniendo el usuario'
      })
    }
    return res.status(200).send({ token: servicejwt.createToken(user) })
  })
}

module.exports = {
  register,
  index,
  login
}
