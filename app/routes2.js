const express = require('express')
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const routerCervezas = require('./routes/v2/cervezas')
const routerProducts = require('./routes/v2/products')
const routerUsers = require('./routes/v2/users')
const routerCathegories = require('./routes/v2/cathegories')
const routerBebidas = require('./routes/v2/bebidas')

// establecemos nuestra primera ruta, mediante get.
router.get('/', (req, res) => {
  res.json({ mensaje: '¡Bienvenido a nuestra API! con MONGODB' })
})

// para AMBAR --------
/*
const Cerveza = require('./models/v2/Cerveza')
router.get('./ambar', (req, res) => {
  const miCerveza = new Cerveza({ nombre: 'Ambar' })
  miCerveza.save((err, miCerveza) => {
    if (err) return console.error(err)
    console.log(`Guardada en bbdd ${miCerveza.nombre}`)
  })
}) */

router.use('/cervezas', routerCervezas)
router.use('/products', routerProducts)
router.use('/users', routerUsers)
router.use('/cathegories', routerCathegories)
router.use('/bebidas', routerBebidas)
module.exports = router
