const express = require('express')
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const bebidaController = require('../../controllers/v2/bebidaController')

router.get('/', (req, res) => {
  //console.log('rutas de cervezas2')
  bebidaController.index(req, res)
})
router.get('/search', (req, res) => {
  // console.log('cerveza encontrada')
  bebidaController.search(req, res)
})

router.get('/:id', (req, res) => {
  //console.log('cerveza')
  bebidaController.show(req, res)
})
router.post('/', (req, res) => {
  //console.log('Crear una cerveza')
  bebidaController.create(req, res)
})
router.put('/:id', (req, res) => {
    bebidaController.update(req, res)
})
module.exports = router