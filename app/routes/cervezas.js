const express = require('express')
// para establecer las distintas rutas, necesitamos instanciar el express router
const router = express.Router()
const cervezaController = require('../controllers/cervezaController.js')

router.get('/', (req, res) => {
  // console.log('rutas de cervezas')
  // let page = req.query.page ? req.query.page : 1;
  // let page = req.query.page || 1;
  cervezaController.index(req, res)
  // res.json({ mensaje: `Lista de cervezas, página ${page}` })
})

router.get('/:id', (req, res) => {
  // console.log('rutas de show cervezas')
  cervezaController.show(req, res)
  // res.json({ mensaje: `¡A beber ${req.params.id}!` })
})

router.post('/', (req, res) => {
  cervezaController.store(req, res)
})

router.delete('/:id', (req, res) => {
  cervezaController.destroy(req, res)
})

router.put('/:id', (req, res) => {
  cervezaController.update(req, res)
})

module.exports = router
