const conn = require('../config/dbconnection.js')

const index = function (callback) {
  const sql = 'SELECT * FROM cervezas'
  conn.query(sql, (err, res, fields) => {
    if (err) {
      console.log(err)
      callback(500, err)
    } else {
      // console.log(res)
      callback(200, res, fields)
    }
  })
}

const find = function (id, callback) {
  const sql = 'SELECT * FROM cervezas WHERE id= ? '
  conn.query(sql, [id], (err, res) => {
    if (err) {
      console.log(err)
      callback(500, err)
    } else {
      // console.log(res)
      callback(200, res)
    }
  })
}

const create = (cerveza, callback) => {
  const sql =
    'INSERT INTO cervezas(name, container, alcohol, price) VALUES (?,?,?,?)'
  conn.query(
    sql,
    [cerveza.name, cerveza.description, cerveza.alcohol, cerveza.price],
    (err, res) => {
      callback(err, res)
    }
  )
}
const update = (cerveza, callback) => {
  const sql =
    'UPDATE cervezas SET name=?, container=?, alcohol=?, price=? WHERE id=?'
  conn.query(
    sql,
    [
      cerveza.name,
      cerveza.container,
      cerveza.alcohol,
      cerveza.price,
      cerveza.id
    ],
    (err, res) => {
      if (err) {
        console.log(err)
        callback(500, err)
      } else {
        // console.log(res)
        callback(200, res)
      }
    }
  )
}

const destroy = (id, callback) => {
  const sql = 'DELETE FROM cervezas WHERE id=?'
  conn.query(sql, [id], (err, res) => {
    if (err) {
      console.log(err)
      callback(500, err)
    } else {
      // console.log(res)
      callback(200, res)
    }
  })
}

module.exports = {
  index,
  find,
  create,
  update,
  destroy
}
