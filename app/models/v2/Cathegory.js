const mongoose = require('mongoose')
const Schema = mongoose.Schema

const cathegorySchema = new Schema({
  nombre: {
    type: String,
    required: true
  },
  descripción: String
  
})

const Cathegory = mongoose.model('Cathegory', cathegorySchema)

module.exports = Cathegory