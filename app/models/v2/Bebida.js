const mongoose = require('mongoose')
const Schema = mongoose.Schema

const bebidaSchema = new Schema({
  nombre: {
    type: String,
    required: true
  },
  precio: String,
  cathegory: String
})

const Bebida = mongoose.model('Bebida', bebidaSchema)

module.exports = Bebida