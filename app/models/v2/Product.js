const mongoose = require('mongoose')

const productSchema = mongoose.Schema({
  // _id: mongoose.Schema.Types.ObjectId,
  name: {
    type: String,
    maxlength: 20,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    maxlength: 255
  },
  created: {
    type: Date,
    default: Date.now
  }
})

const Product = mongoose.model('Product', productSchema)

module.exports = Product
